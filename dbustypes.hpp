/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <QtCore>
#include <QtDBus>

/** List of the unsigned integers: Used for main window queries */
typedef QList<uint> QUIntList;

/** WorkSpace
 * Used to identify the workspace of a view
 * Used to identify the current workspace of an output
 */
typedef struct WorkSpace_t {
    int row    = 0;
    int column = 0;
} WorkSpace;

/** WorkSpaceGrid
 * Used to identify grid size of the workspaces of an output
 */
typedef struct WorkSpace_Grid_t {
    int rows    = 0;
    int columns = 0;
} WorkSpaceGrid;

/** List of WorkSpaces: To identify the WorkSpaces in which a view lives */
typedef QList<WorkSpace> WorkSpaces;

Q_DECLARE_METATYPE( QUIntList );
Q_DECLARE_METATYPE( WorkSpace );
Q_DECLARE_METATYPE( WorkSpaces );
Q_DECLARE_METATYPE( WorkSpaceGrid );

QDBusArgument &operator<<( QDBusArgument& argument, const WorkSpace& ws );
const QDBusArgument &operator>>( const QDBusArgument& argument, WorkSpace& ws );

QDBusArgument &operator<<( QDBusArgument& argument, const WorkSpaceGrid& ws );
const QDBusArgument &operator>>( const QDBusArgument& argument, WorkSpaceGrid& ws );

bool operator==( const WorkSpace& lhs, const WorkSpace& rhs );
