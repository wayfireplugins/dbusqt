/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include "Global.hpp"
#include "WorkspaceAdaptor.hpp"
#include "dbustypes.hpp"

#include <QtCore/QMetaObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>

/* == Helper Functions == */

WorkspaceAdaptor::WorkspaceAdaptor( QObject *parent ) : QDBusAbstractAdaptor( parent ) {
    setAutoRelaySignals( true );
}


WorkspaceAdaptor::~WorkspaceAdaptor() {
}


/** Query the WorkSpaceGrid of the active output */
WorkSpaceGrid WorkspaceAdaptor::QueryWorkspaceGrid() {
    wf::output_t  *output = core.seat->get_active_output();
    WorkSpaceGrid wsg;

    if ( output ) {
        wf::dimensions_t grid = output->wset()->get_workspace_grid_size();
        wsg = WorkSpaceGrid{ grid.height, grid.width };
    }

    return wsg;
}


/** Query the active WorkSpace of the active output */
WorkSpace WorkspaceAdaptor::QueryActiveWorkspace() {
    wf::output_t *output = core.seat->get_active_output();
    WorkSpace    ws;

    if ( output ) {
        wf::point_t _ws = output->wset()->get_current_workspace();
        ws = WorkSpace{ _ws.y, _ws.x };
    }

    return ws;
}


/** Change the WorkSpace of the active output */
void WorkspaceAdaptor::ChangeWorkspace( WorkSpace ws ) {
    wf::wl_idle_call *idle_call = new wf::wl_idle_call;

    idle_call->run_once(
        [ = ] () {
            wf::output_t *output = core.seat->get_active_output();

            if ( output ) {
                output->wset()->request_workspace( { ws.column, ws.row } );
            }

            delete idle_call;
        }
    );
}


/** Present the current workspace views */
void WorkspaceAdaptor::PresentWorkspaceViews() {
    /** To be replaced with IPC calls */
}


/** Present the active output's views */
void WorkspaceAdaptor::PresentActiveOutputViews() {
    /** To be replaced with IPC calls */
}


/** Move view @view_id to workspace @ws - changes the active workspace */
void WorkspaceAdaptor::MoveWithViewToWorkspace( uint view_id, WorkSpace ws ) {
    wf::wl_idle_call *idle_call = new wf::wl_idle_call;

    idle_call->run_once(
        [ = ] () {
            wayfire_toplevel_view view = get_view_from_id( view_id );

            if ( not view ) {
                return;
            }

            wf::output_t *output = core.seat->get_active_output();

            if ( output ) {
                output->wset()->move_to_workspace( view, { ws.column, ws.row } );
                output->wset()->request_workspace( { ws.column, ws.row } );
            }

            delete idle_call;
        }
    );
}


/** Send view @view_id to workspace @ws - retains the active workspace */
void WorkspaceAdaptor::SendViewToWorkspace( uint view_id, WorkSpace ws ) {
    wf::wl_idle_call *idle_call = new wf::wl_idle_call;

    idle_call->run_once(
        [ = ] () {
            wayfire_toplevel_view view = get_view_from_id( view_id );

            if ( not view ) {
                return;
            }

            wf::output_t *output = core.seat->get_active_output();

            if ( output ) {
                output->wset()->move_to_workspace( view, { ws.column, ws.row } );
            }


            delete idle_call;
        }
    );
}


/** List all the views of the current workspace */
QUIntList WorkspaceAdaptor::QueryActiveWorkspaceViews() {
    QUIntList toplevels;

    wf::output_t *output = core.seat->get_active_output();

    for ( wayfire_toplevel_view view: output->wset()->get_views( wf::WSET_CURRENT_WORKSPACE | wf::WSET_SORT_STACKING | wf::WSET_MAPPED_ONLY ) ) {
        toplevels << view->get_id();
    }

    return toplevels;
}


/** List all the views of the current workspace */
QUIntList WorkspaceAdaptor::QueryWorkspaceViews( WorkSpace ws ) {
    QUIntList toplevels;

    wf::output_t *output = core.seat->get_active_output();
    wf::point_t  _ws     = { ws.column, ws.row };

    for ( wayfire_toplevel_view view: output->wset()->get_views( wf::WSET_SORT_STACKING | wf::WSET_MAPPED_ONLY ) ) {
        if ( output->wset()->view_visible_on( view, _ws ) ) {
            toplevels << view->get_id();
        }
    }

    return toplevels;
}


/** Query the Workspaces on which the view @view_id is visible */
WorkSpaces WorkspaceAdaptor::QueryViewWorkspaces( uint view_id ) {
    WorkSpaces   wslist;
    wf::output_t *output = core.seat->get_active_output();

    if ( not output ) {
        return wslist;
    }

    wayfire_toplevel_view view = get_view_from_id( view_id );

    if ( not view ) {
        return wslist;
    }

    wf::dimensions_t grid = output->wset()->get_workspace_grid_size();

    for ( int x = 0; x < grid.height; x++ ) {
        for ( int y = 0; y < grid.width; y++ ) {
            if ( output->wset()->view_visible_on( view, { x, y } ) ) {
                wslist << WorkSpace{ y, x };
            }
        }
    }

    return wslist;
}
