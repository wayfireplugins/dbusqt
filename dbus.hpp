/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include "Global.hpp"
#include "dbustypes.hpp"

class WayfireDBusQt : public QObject {
    Q_OBJECT;

    public:
        WayfireDBusQt();

    Q_SIGNALS:
        /** A new output was added to the compositor */
        void OutputAdded( uint output_id );

        /** An output was disconnected from the compositor */
        void OutputRemoved( uint output_id );

        /** Current output changed. @output_id is the new output */
        void OutputChanged( uint output_id );

        /** The current workspace of output @output_id changed */
        void OutputWorkspaceChanged( uint output_id, WorkSpace ws );

        /** The number of rows and columns of output @output_id changed */
        void OutputWorkspaceGridChanged( uint output_id, WorkSpaceGrid wsg );

        /** A view was added */
        void ViewAdded( uint view_id );

        /** A view was removed */
        void ViewClosed( uint view_id );

        /** A view's output id changed */
        void ViewAppIdChanged( uint view_id, QString new_title );

        /** A view's title changed */
        void ViewTitleChanged( uint view_id, QString new_title );

        /** A view gained/lost demands attention changed */
        void ViewAttentionChanged( uint view_id, bool attention );

        /** A view gained/lost focus */
        void ViewFocusChanged( uint view_id );

        /** A view gained/lost sticky */
        void ViewStickyChanged( uint view_id, bool sticky );

        /** A view was minimized/restored */
        void ViewMinimizedChanged( uint view_id, bool minimized );

        /** A view was maximized/restored */
        void ViewMaximizedChanged( uint view_id, bool maximized );

        /** A view was fullscreened/restored */
        void ViewFullscreenChanged( uint view_id, bool fullscreen );

        /** A view was moved from one output to another */
        void ViewOutputMoved( uint view_id, uint oldOutput, uint newOutput );

        /** A view was moved from one workspace to another */
        void ViewWorkspaceChanged( uint view_id, WorkSpace ws );
};

class WayfireDBusQtPlugin : public wf::plugin_interface_t {
    public:
        void init() override;
        void fini() override;
};
