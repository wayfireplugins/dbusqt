/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include "dbustypes.hpp"

// Marshall the WorkSpace data into a D-Bus argument
QDBusArgument &operator<<( QDBusArgument& argument, const WorkSpace& ws ) {
    argument.beginStructure();
    argument << ws.row;
    argument << ws.column;
    argument.endStructure();
    return argument;
}


// Retrieve the WorkSpace data from the D-Bus argument
const QDBusArgument &operator>>( const QDBusArgument& argument, WorkSpace& ws ) {
    argument.beginStructure();
    argument >> ws.row;
    argument >> ws.column;
    argument.endStructure();
    return argument;
}


// Marshall the WorkSpace data into a D-Bus argument
QDBusArgument &operator<<( QDBusArgument& argument, const WorkSpaceGrid& ws ) {
    argument.beginStructure();
    argument << ws.rows;
    argument << ws.columns;
    argument.endStructure();
    return argument;
}


// Retrieve the WorkSpace data from the D-Bus argument
const QDBusArgument &operator>>( const QDBusArgument& argument, WorkSpaceGrid& ws ) {
    argument.beginStructure();
    argument >> ws.rows;
    argument >> ws.columns;
    argument.endStructure();
    return argument;
}


bool operator==( const WorkSpace& lhs, const WorkSpace& rhs ) {
    return ( (lhs.row == rhs.row) && (lhs.column == rhs.column) );
}


bool operator==( const WorkSpaceGrid& lhs, const WorkSpaceGrid& rhs ) {
    return ( (lhs.rows == rhs.rows) && (lhs.columns == rhs.columns) );
}
